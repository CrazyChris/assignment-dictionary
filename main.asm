%include "colon.inc"
%include "lib.inc"
section .rodata
%include "words.inc"
length_error_message: db "Too long key", 0
key_error_message: db "No such key", 0

section .bss
buffer: resb 255

section .text
global _start
extern find_word

_start:
    mov rdi, buffer
    mov rsi, 255
    call read_word
    test rax, rax
    jz .length_error
    push rdx        ; saving key length
    mov rdi, rax
    mov rsi, START_INDEX
    call find_word
    test rax, rax
    jz .no_key_error
    pop rdx                     
    lea rax, [rax + 8 + rdx + 1]        ; if entry was found, shift to the start index of value string
    mov rdi, rax
    jmp .printing

.length_error:
    mov rdi, length_error_message 
    jmp .print_err

.no_key_error:
    mov rdi, key_error_message

.print_err:
    call print_error
    jmp .exit

.printing:
    call print_string

.exit:
    call print_newline
    call exit
