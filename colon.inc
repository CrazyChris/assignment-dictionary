%define START_INDEX 0
%macro colon 2
    %if START_INDEX == 0
        %2: dq 0
    %else 
        %2: dq START_INDEX
    %endif
    db %1, 0
    %define START_INDEX %2

%endmacro