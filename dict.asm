global find_word
%include "lib.inc"
section .text

; Arguments: rdi - string pointer, rsi - dictionary start index.
; Searches through dictionary for the value given and returns 
; address of entry with given string.
; Returns: addres - if word is in a dictionary, 0 otherwise
find_word:
    test rsi, rsi
    jz .returning
    push rdi
    push rsi
    lea rsi, [rsi + 8]    
    call string_equals    ; comparing keys, if equals rax = 1, otherwise rax = 0 
    pop rsi
    pop rdi
    test rax, rax               
    jnz .returning               ; if rax = 1 then rsi is entry needed
    mov rsi, [rsi]              ; else move on to the next entry     
    jmp find_word               ; if next entry equals 0 then no word was found

.returning:
    mov rax, rsi
    ret


